# Find Pages By Template

A tiny plugin that allows you to search for all pages that use a specific template.

Handy for use cases such as:

 - Pushing a template change live, and want to check associated pages for issues
 - Have lots of individual templates for split testing and want to see which pages and using what
 
 If you have any issues or suggestions, please [create an issue](https://gitlab.com/nomaki/find-pages-by-template/issues/new), or contact me via Twitter [@nomaki](https://twitter.com/nomaki)